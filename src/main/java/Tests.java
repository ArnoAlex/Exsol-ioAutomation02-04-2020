import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.ArrayList;

public class Tests {
    WebDriver driver;


    @Before
    public void setUp() throws InterruptedException {
        driver = new ChromeDriver();
        driver.get("http://exsol.io");
        Thread.sleep(3000);
    }

    /***************************************************************/

    @Test
    /**001**/
    public void clickOnAboutUsButton() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[2]/a")).click();
        Thread.sleep(5000);
        String url = "http://exsol.io/#about";
        if (url.equals(driver.getCurrentUrl())) {
            System.out.println("clickOnAboutUsButton test is True");
        } else {
            System.out.println("clickOnAboutUsButton test is False");
        }
    }

    /***************************************************************/

    @Test
    /**002**/
    public void clickOnProductsButton() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[3]/a")).click();
        Thread.sleep(3000);
        String url = "http://exsol.io/#products";
        if (url.equals(driver.getCurrentUrl())) {
            System.out.println("clickOnProductsButton test is True");
        } else {
            System.out.println("clickOnProductsButton test is False");
        }
    }

    /***************************************************************/

    @Test
    /**003**/
    public void clickOnTeamButton() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[4]/a")).click();
        Thread.sleep(4000);
        String url = "http://exsol.io/#team_board";
        if (url.equals(driver.getCurrentUrl())) {
            System.out.println("clickOnTeamButton test is True");
        } else {
            System.out.println("clickOnTeamButton test is False");
        }
    }

    /***************************************************************/

    @Test
    /**004**/
    public void clickOnBlogButton() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[5]/a")).click();
        Thread.sleep(5000);
        String url = "http://exsol.io/#media_blog";
        if (url.equals(driver.getCurrentUrl())) {
            System.out.println("clickOnBlogButton test is True");
        } else {
            System.out.println("clickOnBlogButton test is False");
        }
    }

    /***************************************************************/

    @Test
    /**005**/
    public void clickOnPartnersButton() throws InterruptedException {

        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[6]/a")).click();
        Thread.sleep(5000);
        String url = "http://exsol.io/#our_Partners";
        if (url.equals(driver.getCurrentUrl())) {
            System.out.println("clickOnPartnersButton test is True");
        } else {
            System.out.println("clickOnPartnersButton test is False");
        }
    }

    /***************************************************************/

    @Test
    /**006**/
    public void clickOnContactusButton() throws InterruptedException {

        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[7]/a")).click();
        Thread.sleep(3000);
        String url = "http://exsol.io/#our_Partners";
        if (url.equals(driver.getCurrentUrl())) {
            System.out.println("clickOnContactusButton test is True");
        } else {
            System.out.println("clickOnContactusButton test is False");
        }
    }

    /***************************************************************/

    @Test
    /**007**/
    public void clickOnHamburgerMenu() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/div")).click();
        Thread.sleep(5000);
        if (driver.findElement(By.className("hamburgerBlok")).isDisplayed()) {
            System.out.println("clickOnHamburgerMenu test is True");
        } else

            System.out.println("clickOnHamburgerMenu test is False");

    }

    /***************************************************************/

    @Test
    /**008**/
    public void clickOnLinkedinButtonHamburgerMenu() throws InterruptedException {

        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/div")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[2]/div/div[3]/div[4]/a[1]")).click();
        Thread.sleep(5000);
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
        Thread.sleep(4000);
        System.out.println(driver.getTitle());
        String currentTitle1 = "Sign Up | LinkedIn";
        String currentTitle2 = "Exsol LLC | LinkedIn";
        String currentTitle3 = "Security Verification | LinkedIn";

        if (currentTitle1.equals(driver.getTitle()) || currentTitle2.equals(driver.getTitle()) || currentTitle3.equals(driver.getTitle())) {
            System.out.println(" clickOnLinkedinButtonHamburgerMenu method is True");
        } else
            System.out.println(" clickOnLinkedinButtonHamburgerMenu method is False");
        Thread.sleep(5000);

    }

    /***************************************************************/

    @Test
    /**009**/
    public void clickOnFacebookButtonHamburgerMenu() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/div")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[2]/div/div[3]/div[4]/a[2]")).click();
        Thread.sleep(5000);
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
        Thread.sleep(4000);
        System.out.println(driver.getCurrentUrl());
        String curren = "https://www.facebook.com/exsol.io/";
        if (curren.equals(driver.getCurrentUrl())) {
            System.out.println(" clickOnFacebookButtonHamburgerMenu method is True");
        } else
            System.out.println(" clickOnFacebookButtonHamburgerMenu method is False");

    }

    /***************************************************************/

    @Test
    /**010**/
    public void clickOnReadMoreButtonInProductsWhiteLabel() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[3]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("productBTN")).click();
        Thread.sleep(5000);
        driver.findElement(By.className("titales")).isDisplayed();

    }

    /***************************************************************/

    @Test
    /**011**/
    public void clickOnReadMoreButtonInProductsExchangeAPI() throws InterruptedException {

        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[3]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"scrolleftP\"]/div[2]/a")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#routerContent > div.products_wrapper.products_component_big_div > div > div.exchangeDiv > div.exchangeDiv__img > img")).isDisplayed();  // image adress "like 8"
        System.out.println("mtav clickOnReadMoreButtonInProductsExchangeAPI Test");

    }

    /***************************************************************/

    @Test
    /**012**/
    public void clickOnReadMoreButtonInProductsCasino() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[3]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"scrolleftP\"]/div[3]/a")).click();
        Thread.sleep(5000);
        driver.findElement(By.className("constructorTree")).isDisplayed();
        System.out.println("mtav clickOnReadMoreButtonInProductsCasino Test");


    }

    /***************************************************************/

    @Test
    /**013**/
    public void clickOnLeftAndRightArrowsInTeamboard() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[4]/a")).click();
        Thread.sleep(2000);
        for (int i = 0; i < 6; i++) {
            driver.findElement(By.className("aroowRight")).click();
            Thread.sleep(2000);
        }
        driver.findElement(By.xpath("//*[contains(text(), 'Arnold Aleksandryan')]")).isDisplayed();
        for (int j = 0; j < 6; j++) {
            driver.findElement(By.className("aroowLeft")).click();
            Thread.sleep(2000);

        }
        driver.findElement(By.xpath("//*[contains(text(), 'Gagik Papoyan')]")).isDisplayed();
    }

    /***************************************************************/

    @Test
    /**014**/
    public void clickOntheCrossPlatformTradingInBlog() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[5]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"media_blog\"]/div/div/div/div[2]/div[1]/a/div")).click();
        driver.findElement(By.cssSelector("#routerContent > section > div.media_blog_div.wrapper > div > div > div.media_blog_img > img")).isDisplayed();
        Thread.sleep(5000);

    }

    /***************************************************************/
    @Test
    /**015**/
    public void clickOntheExchangeInBlog() throws InterruptedException {

        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[5]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"media_blog\"]/div/div/div/div[2]/div[2]/a/div")).click();
        driver.findElement(By.cssSelector("#routerContent > section > div.media_blog_div.wrapper > div > div > div.media_blog_img > img")).isDisplayed();
        Thread.sleep(5000);

    }

    /***************************************************************/
    @Test
    /**016**/
    public void clickOnthePowerfulserviceInBlog() throws InterruptedException {

        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[5]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"media_blog\"]/div/div/div/div[2]/div[3]/a/div")).click();
        driver.findElement(By.cssSelector("#routerContent > section > div.media_blog_div.wrapper > div > div > div.media_blog_img > img")).isDisplayed();
        Thread.sleep(5000);

    }

    /***************************************************************/

    @Test
    /**017**/
    public void clickOntheMultiSignatureInBlog() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[5]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"media_blog\"]/div/div/div/div[2]/div[4]/a/div")).click();
        driver.findElement(By.cssSelector("#routerContent > section > div.media_blog_div.wrapper > div > div > div.media_blog_img > img")).isDisplayed();
        Thread.sleep(5000);

    }

    /***************************************************************/

    @Test
    /**018**/
    public void clickOnOurPartnersLogosLinkBetconstruct() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[6]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"scrollleftP\"]/div[1]/a")).click();
        Thread.sleep(5000);
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
        Thread.sleep(4000);
        System.out.println(driver.getCurrentUrl());
        String curren = "https://www.betconstruct.com/";
        if (curren.equals(driver.getCurrentUrl())) {
            System.out.println(" clickOnOurPartnersLogosLinkBetconstruct method is True");
        } else
            System.out.println(" clickOnOurPartnersLogosLinkBetconstruct method is False");
        Thread.sleep(5000);

    }


    /***************************************************************/

    @Test
    /**019**/
    public void clickOnOurPartnersLogosLinkBitex() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[6]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"scrollleftP\"]/div[2]/a")).click();
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
        Thread.sleep(4000);
        System.out.println(driver.getCurrentUrl());
        String curren = "https://www.bitexuae.com/";
        if (curren.equals(driver.getCurrentUrl())) {
            System.out.println(" clickOnOurPartnersLogosLinkBitex method is True");
        } else
            System.out.println(" clickOnOurPartnersLogosLinkBitex method is False");
        Thread.sleep(5000);

    }

    /***************************************************************/

    @Test
    /**020**/
    public void clickOnAboutUsButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(2) > a")).click();

    }

    /***************************************************************/

    @Test
    /**021**/
    public void clickOnHomeButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(1) > a")).click();

    }

    /***************************************************************/

    @Test
    /**022**/
    public void clickOnProductsButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(3) > a")).click();

    }

    /***************************************************************/

    @Test
    /**023**/
    public void clickOnTeamButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(4) > a")).click();

    }

    /***************************************************************/

    @Test
    /**024**/
    public void clickOnBlogButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(5) > a")).click();

    }

    /***************************************************************/

    @Test
    /**025**/
    public void clickOnPartnersButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(6) > a")).click();

    }

    /***************************************************************/

    @Test
    /**026**/
    public void clickOnContactUsButtonInFooter() throws InterruptedException {
        driver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5500)", "");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("#our_Partners > div.contactsDiv > div.contacts > div.contacts_div > div:nth-child(2) > ul > li:nth-child(7) > a")).click();

    }

    /***************************************************************/

    @Test
    /**027**/
    public void smsSendingFormInWhiteLabelPage() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"page\"]/section/div[1]/div/div/div[2]/nav/ul/li[3]/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("productBTN")).click();
        driver.findElement(By.xpath("//*[@id=\"buyForm\"]/div[1]/input")).sendKeys("Hrazmik");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"buyForm\"]/div[2]/input")).sendKeys("1@mail.ru");
        Thread.sleep(4000);
        driver.findElement(By.xpath("//*[@id=\"buyForm\"]/textarea")).sendKeys("Tyom mi zarmana )))");
        driver.findElement(By.className("backBtn")).click();
        Thread.sleep(3000);
        driver.switchTo().alert().dismiss();
        driver.findElement(By.className("productMassige")).isDisplayed();

    }

    /***************************************************************/


    @After
    public void tearDown() throws InterruptedException {
        if (driver != null)
            driver.quit();
        Thread.sleep(5000);

    }

}
